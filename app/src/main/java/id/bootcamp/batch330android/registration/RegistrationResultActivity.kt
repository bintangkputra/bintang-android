package id.bootcamp.batch330android.registration

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import id.bootcamp.batch330android.MainActivity
import id.bootcamp.batch330android.R

class RegistrationResultActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_registration_result)

        //ambil data yang dikirim activity sebelumnya
        val firstName = intent.extras?.getString("keyFirstName")
        val lastName = intent.extras?.getString("keyLastName")
        val email = intent.extras?.getString("keyEmail")
        val address = intent.extras?.getString("keyAddress")
        val gender = intent.extras?.getString("keyGender")
        val language = intent.extras?.getStringArrayList("keyLanguage")
        val state = intent.extras?.getString("keyState")

        //ambil obyek dari layout
        val textFullName = findViewById<TextView>(R.id.viewName)
        val textEmail = findViewById<TextView>(R.id.viewEmail)
        val textAddress = findViewById<TextView>(R.id.viewAddress)
        val textGender = findViewById<TextView>(R.id.viewGendel)
        val textLanguage = findViewById<TextView>(R.id.viewLanguage)
        val textState = findViewById<TextView>(R.id.viewState)

        //set text data dari activity sebelumnya
        textFullName.text = "Full Name: $firstName $lastName"
        textGender.text = "Gender: $gender"
        textLanguage.text = "Language: $language"
        textEmail.text = "Email: $email"
        textAddress.text = "Address: $address"
        textState.text = "State: $state"

        val btnHome = findViewById<Button>(R.id.btnHome)
        btnHome.setOnClickListener {
            //pindah ke page awal
            val home = Intent(this, MainActivity::class.java)

            //fungsi tambahan untuk clear atau menambah activity
            home.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
            startActivity(home)
        }
    }
}