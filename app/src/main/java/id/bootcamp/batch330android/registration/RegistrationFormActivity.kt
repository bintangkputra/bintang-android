package id.bootcamp.batch330android.registration

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ArrayAdapter
import android.widget.Button
import android.widget.CheckBox
import android.widget.EditText
import android.widget.RadioButton
import android.widget.Spinner
import android.widget.Toast
import id.bootcamp.batch330android.R
import id.bootcamp.batch330android.utils.isValidEmail

class RegistrationFormActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //perintah memasang layout
        setContentView(R.layout.activity_registration_form)

        //ambil semua objek yg ada di form
        val editFirstname = findViewById<EditText>(R.id.etFirstName)
        val editLastName = findViewById<EditText>(R.id.etLastName)
        val formMale = findViewById<RadioButton>(R.id.rbMale)
        val formFemale = findViewById<RadioButton>(R.id.rbFemale)
        val checkIndonesia = findViewById<CheckBox>(R.id.cbIndonesia)
        val checkEnglish = findViewById<CheckBox>(R.id.cbEnglish)
        val editEmail = findViewById<EditText>(R.id.etEmail)
        val editAddress = findViewById<EditText>(R.id.address)
        val nation = findViewById<Spinner>(R.id.spinnerState)
        val submit = findViewById<Button>(R.id.btnRegisterNow)

        //ambil array string dari resources
        val states = resources.getStringArray(R.array.states)

        //mengisi spinner / dropdown
        val adaptor = ArrayAdapter(this, android.R.layout.simple_list_item_1, states)
        nation.adapter = adaptor

        //buat clicklistener button submit
        submit.setOnClickListener {
            //ambil value dari form

            //edit text
            val firstName = editFirstname.text.toString()
            val lastName = editLastName.text.toString()
            val email = editEmail.text.toString()
            val address = editAddress.text.toString()

            //radio button
            var gender = ""
            if (formMale.isChecked) {
                gender = "Male"
            } else if (formFemale.isChecked){
                gender = "Female"
            }

            //checkbox
            //perintah deklarasi membuat list kosong
            var listLanguage = ArrayList<String>()
            //perintah buat menambahkan data di listLanguage
            if (checkIndonesia.isChecked) {
                listLanguage.add("Indonesia")
            }
            if (checkEnglish.isChecked) {
                listLanguage.add("English")
            }

            //spinner / dropdwon
            val state = nation.selectedItem.toString()

            //logic validation
            //firstname gak boleh kosong
            if (firstName.isBlank()) {
                Toast.makeText(
                    this@RegistrationFormActivity,
                    "Input First Name",
                    Toast.LENGTH_SHORT
                ).show()
                return@setOnClickListener
            }

            //gender wajib dipilih
            if (formMale.isChecked) {

            } else if (formFemale.isChecked){

            } else {
                Toast.makeText(
                    this@RegistrationFormActivity,
                    "Select Gender!",
                    Toast.LENGTH_SHORT
                ).show()
                return@setOnClickListener
            }

            //language auto fill-i
            if (listLanguage.isNullOrEmpty()) {
                Toast.makeText(
                    this@RegistrationFormActivity,
                    "Choose Language!",
                    Toast.LENGTH_SHORT
                ).show()
                return@setOnClickListener
            }

            //email harus valid
            val isEmailVaild = isValidEmail(email)
            if (isEmailVaild == false){
                Toast.makeText(
                    this@RegistrationFormActivity,
                    "Invalid Email",
                    Toast.LENGTH_SHORT
                ).show()
                return@setOnClickListener
            }

            //address harus dipilih
            if (address.isBlank()) {
                Toast.makeText(
                    this@RegistrationFormActivity,
                    "Input Address",
                    Toast.LENGTH_SHORT
                ).show()
                return@setOnClickListener
            }

            val intent = Intent(this, RegistrationResultActivity::class.java)
            intent.putExtra("keyFirstName", firstName)
            intent.putExtra("keyLastName", lastName)
            intent.putExtra("keyEmail", email)
            intent.putExtra("keyAddress", address)
            intent.putExtra("keyGender", gender)
            intent.putExtra("keyLanguage", listLanguage)
            intent.putExtra("keyState", state)
            startActivity(intent)
        }
    }
}