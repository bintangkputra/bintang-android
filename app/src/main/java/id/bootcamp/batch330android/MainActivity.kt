package id.bootcamp.batch330android

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.Toast
import id.bootcamp.batch330android.activitystack.Stack1Activity
import id.bootcamp.batch330android.activitystack.Stack2Activity
import id.bootcamp.batch330android.greeding.GreedingSituation
import id.bootcamp.batch330android.registration.RegistrationFormActivity

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //set layout main activity
        setContentView(R.layout.activity_main)

        //ambil obyek button
        val btnStackActivity = findViewById<Button>(R.id.btnActivityStack)
        val btnRegister = findViewById<Button>(R.id.btnRegister)
        val btnGreeding = findViewById<Button>(R.id.btnSurprise)

        //atur button setelah di klik
        btnStackActivity.setOnClickListener {
//            Toast.makeText(this,
//                "Button Activity Clicked",
//                Toast.LENGTH_SHORT).show()

            // buat object Intent untuk navigasi dari apa ke apa
            // navigasi dari apa ke apa

            val intent = Intent(this, Stack1Activity::class.java)
            startActivity(intent)
        }

        btnRegister.setOnClickListener {
            //kalo misal button register dklik
            //jalankan perintah buka halaman selanjutnya
            val pilih = Intent(this, RegistrationFormActivity::class.java)
            startActivity(pilih)
        }

        btnGreeding.setOnClickListener {
            val intent = Intent(this, GreedingSituation::class.java)
            startActivity(intent)
        }

        Log.d("activity_lifecycle","OnCreate Terpangil")
    }

    override fun onStart() {
        super.onStart()
        Log.d("activity_lifecycle","OnStart Terpangil")
    }

    override fun onResume() {
        super.onResume()
        Log.d("activity_lifecycle","OnResume Terpangil")
    }

    override fun onPause() {
        super.onPause()
        Log.d("activity_lifecycle","OnPause Terpangil")
    }

    override fun onStop() {
        super.onStop()
        Log.d("activity_lifecycle","OnStop Terpangil")
    }

    override fun onDestroy() {
        super.onDestroy()
        Log.d("activity_lifecycle","OnDestroy Terpangil")
    }
}