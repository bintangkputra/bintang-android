package id.bootcamp.batch330android.activitystack

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.Toast
import id.bootcamp.batch330android.MainActivity
import id.bootcamp.batch330android.R

class Stack2Activity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_stack2)

        //ambil obyek button
        val btnGoMainMenu = findViewById<Button>(R.id.btnGoMainMenu)

        //atur button setelah di klik
        btnGoMainMenu.setOnClickListener {
//            Toast.makeText(this,
//                "Pindah Ke Main Menu",
//                Toast.LENGTH_SHORT).show()
            val pindah = Intent(this, MainActivity::class.java)
            //fungsi tambahan untuk clear atau menambah activity
            pindah.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
            startActivity(pindah)
        }
    }
}