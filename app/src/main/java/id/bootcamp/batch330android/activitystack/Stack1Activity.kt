package id.bootcamp.batch330android.activitystack

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.Toast
import id.bootcamp.batch330android.R

class Stack1Activity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_stack1)

        //ambil obyek button
        val btnGoStack2 = findViewById<Button>(R.id.btnGoStack2)

        //atur button setelah di klik
        btnGoStack2.setOnClickListener {
//            Toast.makeText(this,
//                "Pindah Ke Stack 2 Activity",
//                Toast.LENGTH_SHORT).show()
            val pindah = Intent(this, Stack2Activity::class.java)
            startActivity(pindah)
        }
    }
}