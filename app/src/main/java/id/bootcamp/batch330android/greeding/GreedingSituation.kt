package id.bootcamp.batch330android.greeding

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import id.bootcamp.batch330android.MainActivity
import id.bootcamp.batch330android.R

class GreedingSituation : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_greeding_situation)

        val btnHome = findViewById<Button>(R.id.btnBackHome)

        btnHome.setOnClickListener {
            val intent = Intent(this, MainActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
            startActivity(intent)
        }

        val runningPakdhe = findViewById<TextView>(R.id.runningHello)
        runningPakdhe.isSelected = true
    }
}